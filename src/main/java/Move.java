public class Move {
    private String move;
    private int moveValue;

    public Move (String move, int moveValue) {
        this.move = move;
        this.moveValue = moveValue;
    }

    public void setMove(String move) {
        this.move = move;
    }

    public String getMove() {
        return move;
    }

    public void setMoveValue(int moveValue) {
        this.moveValue = moveValue;
    }

    public int getMoveValue() {
        return moveValue;
    }
}
