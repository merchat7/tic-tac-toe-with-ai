import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class AlgoAIParallel implements AlgoAI {
    private TicTacToeBoard ticTacToeBoard;

    public AlgoAIParallel() {
        ticTacToeBoard = new TicTacToeBoard();
    }

    @Override
    public String getNextMove(int[] board) {
        // As we make new TicTacToeBoard, we need to set it to match the original board
        ticTacToeBoard.setBoard(board.clone()); // Must clone board, otherwise original board will be affected

        String bestMove = "";
        int bestMoveValue = Integer.MIN_VALUE; // Should maximize for O_PLAYER
        List<String> availMoves = getAvailMoves(board);
        List<Future<Integer>> results = new ArrayList<>();
        List<String> moveOfResults = new ArrayList<>();
        int alpha = Integer.MIN_VALUE;
        int beta = Integer.MAX_VALUE;
        for (int i = 0; i < availMoves.size(); i++) {
            String move = availMoves.get(i);
            ticTacToeBoard.playAt(move, Game.O_PLAYER);
            Future<Integer> result = TicTacToe.executor.submit(new MinimaxTask(ticTacToeBoard.getBoard(), 0, Game.X_PLAYER, alpha, beta));
            if (i == 0) { // Wait for the first branch to finish, and then set the alpha & beta for future tasks
                Move moveStore = new Move(bestMove, bestMoveValue);
                alpha = determineBound(result, moveStore, move, alpha);
                bestMove = moveStore.getMove();
                bestMoveValue = moveStore.getMoveValue();
            }
            else {
                moveOfResults.add(move);
                results.add(result);
            }
            ticTacToeBoard.setBoard(board.clone());
        }

        return getBestMove(results, moveOfResults, bestMove, bestMoveValue);
    }

    private int determineBound (Future<Integer> result, Move moveStore, String currentMove, int alpha) {
        try {
            int currentMoveValue = result.get();
            Move newMove = newBestMove(moveStore.getMove(), moveStore.getMoveValue(), currentMove, currentMoveValue);
            moveStore.setMove(newMove.getMove());
            moveStore.setMoveValue(newMove.getMoveValue());
            return Integer.max(alpha, newMove.getMoveValue());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private String getBestMove( List<Future<Integer>> results, List<String> moveOfResults,  String bestMove, int bestMoveValue ) {
        for (int i = 0; i < results.size(); i++) {
            try {
                Future<Integer> result = results.get(i);
                int currentMoveValue = result.get();
                Move newMove = newBestMove(bestMove, bestMoveValue, moveOfResults.get(i), currentMoveValue);
                bestMove = newMove.getMove();
                bestMoveValue = newMove.getMoveValue();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        return bestMove;
    }

    private Move newBestMove(String bestMove, int bestMoveValue, String currentMove, int currentMoveValue) {
        // TODO: Fix bug when trying to randomize move
        if (bestMove.length() == 0 || bestMoveValue < currentMoveValue) {
            return new Move(currentMove, currentMoveValue);
        }
        return new Move(bestMove, bestMoveValue);
    }

    private List<String> getAvailMoves(int[] board) {
        List<String> availMoves = new ArrayList<>();
        for (int i = 0; i < board.length; i++) if (board[i] == ticTacToeBoard.FREE) availMoves.add(ticTacToeBoard.mapIndexToSquare(i));
        return availMoves;
    }
}
