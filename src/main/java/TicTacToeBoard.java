public class TicTacToeBoard {
	static final int FREE = 2;
	static final int X = 3;
	static final int O = 5;

	public int[] getBoard() {
		return board;
	}

	public void setBoard(int[] board) {
		this.board = board;
	}

	private int board [];
	private int vectors [][] =
	    {
		   {0, 1, 2},    // Row 1
	       {3, 4, 5},    // Row 2
	       {6, 7, 8},    // Row 3
	       {0, 3, 6},    // Column 1
	       {1, 4, 7},    // Column 2
	       {2, 5, 8},    // Column 3
	       {0, 4, 8},    // Diagonal 1
	       {2, 4, 6}     // Diagonal 2
	    };

	public TicTacToeBoard() {
		this.reset();
	}

	public void reset() {
		board = new int[] {FREE, FREE, FREE, FREE, FREE, FREE, FREE, FREE, FREE};
	}

	private int getSquare(int index) {
		if (index < 0 | index > 8) throw new IllegalArgumentException("index must be 0-9");
		return board[index];
	}

	/**
	 * getSquare(String square)
	 * Use by the G
	 */
	public int getSquare(String square)
	{
		int index = mapSquareToIndex(square);
		if (index == -1) throw new IllegalArgumentException("Invalid square");
		switch (getSquare(index))
		{
			case X:
				return Square.IS_X;
			case O:
				return Square.IS_O;
			default:
				return Square.IS_FREE;
		}
	}

	public int mapSquareToIndex(String square) {
		switch (square)
		{
			case "A1":
				return 0;
			case "A2":
				return 1;
			case "A3":
				return 2;
			case "B1":
				return 3;
			case "B2":
				return 4;
			case "B3":
				return 5;
			case "C1":
				return 6;
			case "C2":
				return 7;
			case "C3":
				return 8;
			default:
				return -1;
		}
	}

	public String mapIndexToSquare(int index)	{
		switch (index)
		{
			case 0:
				return "A1";
			case 1:
				return "A2";
			case 2:
				return "A3";
			case 3:
				return "B1";
			case 4:
				return "B2";
			case 5:
				return "B3";
			case 6:
				return "C1";
			case 7:
				return "C2";
			case 8:
				return "C3";
			default:
				return "";
		}
	}


	public void playAt(String square, int player)	{
		int index = mapSquareToIndex(square);
		if (index == -1) throw new IllegalArgumentException("Invalid square");
		this.playAt(index, player);
	}

	private void playAt(int index, int player) {
		if (index < 0 | index > 8) throw new IllegalArgumentException("Square must be 0-8");
		if (player < 1 | player > 2)	throw new IllegalArgumentException("Player must be 1 or 2");
		if (board[index] != FREE)	throw new IllegalArgumentException("Square is not empty.");
		if (player == Game.X_PLAYER) board[index] = X;
		else board[index] = O;
	}

	public GameState isGameOver() {
		// check for win
		for (int v = 0; v < 8; v++) {
			int p = getVectorProduct(v);
			if (p == 27) return GameState.WIN;
			if (p == 125) return GameState.LOSE;
	    }

	    // check for draw
	    int freeCount = 0;
        for (int square : board) if (square == FREE) freeCount++;
	    if (freeCount == 0) return GameState.TIE;

	    return GameState.NOT_FINISH;
	}

	private int getVectorProduct(int vector){
		return board[vectors[vector][0]] * board[vectors[vector][1]] * board[vectors[vector][2]];
    }

	public String getNextOMove() {
		AlgoAI algoAI;
		algoAI = new AlgoAIParallel();
		return algoAI.getNextMove(board);
	}

	public String getNextXMove() {
		AlgoAI algoAI;
		algoAI = new AlgoAIMinimax(Game.X_PLAYER);
		return algoAI.getNextMove(board);
	}

	public String toString() {
		return " " +
		       getMark(board[0]) + " | " +
		       getMark(board[1]) + " | " +
		       getMark(board[2]) +
		       "\n-----------\n" +
		       " " +
		       getMark(board[3]) + " | " +
		       getMark(board[4]) + " | " +
		       getMark(board[5]) +
		       "\n-----------\n" +
		       " " +
		       getMark(board[6]) + " | " +
		       getMark(board[7]) + " | " +
		       getMark(board[8]);
    }


	private String getMark(int status) {
		if (status == X)
		    return "X";
		if (status == O)
		    return "O";
		return " ";
	}

}
