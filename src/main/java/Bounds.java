public class Bounds {
    private int alpha;
    private int beta;
    private int value;

    public Bounds (int alpha, int beta, int value) {
        this.alpha = alpha;
        this.beta = beta;
        this.value = value;
    }

    public int getAlpha() {
        return alpha;
    }

    public int getBeta() {
        return beta;
    }

    public int getValue() {
        return value;
    }
}
