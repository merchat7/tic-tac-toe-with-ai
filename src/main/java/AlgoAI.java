public interface AlgoAI {
    String getNextMove (int[] board);
}
