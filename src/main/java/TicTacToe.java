import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class TicTacToe extends JFrame {

	public static void main(String [] args) {
		new TicTacToe();
	}

	private JButton btnA1, btnA2, btnA3, btnB1, btnB2, btnB3, btnC1, btnC2, btnC3;
	private TicTacToeBoard board;
	static ThreadPoolExecutor executor;

	public TicTacToe() {
		board = new TicTacToeBoard(); // TicTacToeBoard contains all game logic
        initGUI();
		executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		//check();
	}

	private void initGUI() {
        this.setSize(300,300);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setTitle("Tic-Tac-Toe");
        this.add(initGamePanel());
        this.setVisible(true);
    }

    private JPanel initGamePanel () {
		JPanel gamePanel = new JPanel();
		gamePanel.setSize(300,300);
		gamePanel.setLayout(new GridLayout(3,3));
		btnA1 = createButton("A1");
		btnA2 = createButton("A2");
		btnA3 = createButton("A3");
		btnB1 = createButton("B1");
		btnB2 = createButton("B2");
		btnB3 = createButton("B3");
		btnC1 = createButton("C1");
		btnC2 = createButton("C2");
		btnC3 = createButton("C3");
		gamePanel.add(btnA1);
		gamePanel.add(btnA2);
		gamePanel.add(btnA3);
		gamePanel.add(btnB1);
		gamePanel.add(btnB2);
		gamePanel.add(btnB3);
		gamePanel.add(btnC1);
		gamePanel.add(btnC2);
		gamePanel.add(btnC3);
		return gamePanel;
	}

	private JButton createButton(String square) {
		JButton btn = new JButton();
		btn.setPreferredSize(new Dimension(50, 50));
		Font f = new Font("Dialog", Font.PLAIN, 72);
		btn.setFont(f);
		btn.addActionListener(e -> btnClick(e, square));
		return btn;
	}

	private void btnClick(ActionEvent e, String square) {
		if (board.getSquare(square) != Square.IS_FREE) return;

		makePlayerMove(e, square); // Player is X
		if (showGameOver(board.isGameOver())) return;

		makeOComputerMove();
		if (showGameOver(board.isGameOver())) return;
	}

	private boolean showGameOver (GameState state) {
		switch (state)
		{
			case WIN:
				JOptionPane.showMessageDialog(null,
						"You won!", "Game Over",
						JOptionPane.INFORMATION_MESSAGE);
				resetGame();
				return true;
			case LOSE:
				JOptionPane.showMessageDialog(null,
						"You lost!", "Game Over",
						JOptionPane.INFORMATION_MESSAGE);
				resetGame();
				return true;
			case TIE:
				JOptionPane.showMessageDialog(null,
						"It's a draw!", "Game Over",
						JOptionPane.INFORMATION_MESSAGE);
				resetGame();
				return true;
		}
		return false;
	}

	private void makePlayerMove(ActionEvent e, String square) {
		JButton btn = (JButton)e.getSource();
		board.playAt(square, Game.X_PLAYER);
		btn.setText("X");
	}

	private void makeOComputerMove() {
		String computerMove = board.getNextOMove();
		board.playAt(computerMove,Game.O_PLAYER);
		draw(computerMove, "O");
	}

	private void makeXComputerMove() {
		String computerMove = board.getNextXMove();
		board.playAt(computerMove,Game.X_PLAYER);
		draw(computerMove, "X");
	}

	private void draw(String move, String letter) {
		switch (move) {
			case "A1":
				btnA1.setText(letter);
				break;
			case "A2":
				btnA2.setText(letter);
				break;
			case "A3":
				btnA3.setText(letter);
				break;
			case "B1":
				btnB1.setText(letter);
				break;
			case "B2":
				btnB2.setText(letter);
				break;
			case "B3":
				btnB3.setText(letter);
				break;
			case "C1":
				btnC1.setText(letter);
				break;
			case "C2":
				btnC2.setText(letter);
				break;
			case "C3":
				btnC3.setText(letter);
				break;
		}
	}

	private void resetGame() {
		board.reset();
		resetButtons();
	}

	private void resetButtons () {
		btnA1.setText("");
		btnA2.setText("");
		btnA3.setText("");
		btnB1.setText("");
		btnB2.setText("");
		btnB3.setText("");
		btnC1.setText("");
		btnC2.setText("");
		btnC3.setText("");
	}

	private void check() {
		while (true) {
			makeXComputerMove();
			if (board.isGameOver() != GameState.NOT_FINISH) {
				if (board.isGameOver() == GameState.WIN || board.isGameOver() == GameState.LOSE) showGameOver(board.isGameOver());
				resetGame();
			}
			makeOComputerMove();
			if (board.isGameOver() != GameState.NOT_FINISH) {
				if (board.isGameOver() == GameState.WIN || board.isGameOver() == GameState.LOSE) showGameOver(board.isGameOver());
				resetGame();
			}
		}
	}
}
