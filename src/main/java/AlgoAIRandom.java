import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AlgoAIRandom implements AlgoAI {
    private TicTacToeBoard ticTacToeBoard;

    public AlgoAIRandom () {
        ticTacToeBoard = new TicTacToeBoard();
    }

    @Override
    public String getNextMove(int[] board) {
        List<String> availMoves = getAvailMoves(board);
        if (!availMoves.isEmpty()) {
            Random rand = new Random();
            int randIndex = rand.nextInt(availMoves.size());
            return availMoves.get(randIndex);
        }
        return "";
    }

    private List<String> getAvailMoves(int[] board) {
        List<String> availMoves = new ArrayList<>();
        for (int i = 0; i < board.length; i++) if (board[i] == ticTacToeBoard.FREE) availMoves.add(ticTacToeBoard.mapIndexToSquare(i));
        return availMoves;
    }
}
