import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AlgoAIMinimax implements AlgoAI {
    private TicTacToeBoard ticTacToeBoard;
    private int count = 0;
    private int currentPlayer;

    public AlgoAIMinimax(int currentPlayer) {
        this.currentPlayer = currentPlayer;
        ticTacToeBoard = new TicTacToeBoard();
    }

    @Override
    public String getNextMove(int[] board) {
        // As we make new TicTacToeBoard, we need to set it to match the original board
        ticTacToeBoard.setBoard(board.clone()); // Must clone board, otherwise original board will be affected

        String bestMove = "";
        int bestMoveValue;
        int otherPlayer;
        if (currentPlayer == Game.O_PLAYER) {
            bestMoveValue = Integer.MIN_VALUE; // Should maximize for O_PLAYER
            otherPlayer = Game.X_PLAYER;
        }
        else {
            bestMoveValue = Integer.MAX_VALUE; // Minimize for X_PLAYER
            otherPlayer = Game.O_PLAYER;
        }
        List<String> availMoves = getAvailMoves(board);
        for (String move : availMoves) {
            ticTacToeBoard.playAt(move, currentPlayer);
            int currentMoveValue = minimax(ticTacToeBoard.getBoard(), 0, otherPlayer, Integer.MIN_VALUE, Integer.MAX_VALUE);
            Move newMove = newBestMove(bestMove, bestMoveValue, move, currentMoveValue);
            bestMove = newMove.getMove();
            bestMoveValue = newMove.getMoveValue();
            ticTacToeBoard.setBoard(board.clone());
        }
        System.out.println("Number of branches tested = " + count);
        return bestMove;
    }

    private int minimax (int[] board, int depth, int player, int alpha, int beta) {
        count += 1; // For performance tracking

        int gameOverValue = getGameOverValue(depth);
        if (gameOverValue != -1) return gameOverValue;

        // TODO: Modify as command line args
        if (depth == 100) return 0; // 0 as didn't return when calling getGameOverValue(depth) so no idea if win/lose for this branch

        return getBestVal(board, depth, player, alpha, beta);
    }

    private int getBestVal(int[] board, int depth, int player, int alpha, int beta) {
        int[] initialBoard = board.clone(); // Must clone here as will be modified when calling playAt(move, player)
        List<String> availMoves = getAvailMoves(board);

        int val;
        int bestVal;
        if (player == Game.O_PLAYER) { // Maximize
            bestVal = Integer.MIN_VALUE;
            for (String move : availMoves) {
                ticTacToeBoard.playAt(move, Game.O_PLAYER);
                val = minimax(ticTacToeBoard.getBoard(), depth+1, Game.X_PLAYER, alpha, beta);
                bestVal = Integer.max(bestVal, val);
                alpha = Integer.max(alpha, bestVal);
                ticTacToeBoard.setBoard(initialBoard.clone());
                if (beta <= alpha) break;
            }
            return bestVal;
        }
        else { // X_PLAYER, Minimize
            bestVal = Integer.MAX_VALUE;
            for (String move : availMoves) {
                ticTacToeBoard.playAt(move, Game.X_PLAYER);
                val = minimax(ticTacToeBoard.getBoard(), depth+1, Game.O_PLAYER, alpha, beta);
                bestVal = Integer.min(bestVal, val);
                beta = Integer.min(beta, bestVal);
                ticTacToeBoard.setBoard(initialBoard.clone());
                if (beta <= alpha) break;
            }
            return bestVal;
        }
    }

    private int getGameOverValue (int depth) {
        GameState state = ticTacToeBoard.isGameOver();
        switch (state) {
            case LOSE: // Player loses = Computer wins
                return 100 - depth;
            case WIN: // Player wins = Computer loses
                return -100 + depth;
            case TIE:
                return 0;
        }
        return -1;
    }

    private Move newBestMove(String bestMove, int bestMoveValue, String currentMove, int currentMoveValue) {
        // If currentValue and bestValue is the same, 50% chance of it getting replaced
        if (bestMove.length() == 0 || ((bestMoveValue == currentMoveValue) && (new Random().nextInt(2) == 0))) {
            return new Move(currentMove, currentMoveValue);
        }
        if (currentPlayer == Game.O_PLAYER && bestMoveValue < currentMoveValue) return new Move(currentMove, currentMoveValue);
        if (currentPlayer == Game.X_PLAYER && bestMoveValue > currentMoveValue) return new Move(currentMove, currentMoveValue);
        return new Move(bestMove, bestMoveValue);
    }

    private List<String> getAvailMoves(int[] board) {
        List<String> availMoves = new ArrayList<>();
        for (int i = 0; i < board.length; i++) if (board[i] == ticTacToeBoard.FREE) availMoves.add(ticTacToeBoard.mapIndexToSquare(i));
        return availMoves;
    }
}
