import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class MinimaxTask implements Callable<Integer> {
    private TicTacToeBoard ticTacToeBoard;
    private int[] board;
    private int depth;
    private int player;
    private int alpha;
    private int beta;

    public MinimaxTask (int[] board, int depth, int player, int alpha, int beta) {
        this.board = board;
        this.depth = depth;
        this.player = player;
        this.alpha = alpha;
        this.beta = beta;
        ticTacToeBoard = new TicTacToeBoard();
        ticTacToeBoard.setBoard(board);
    }

    @Override
    public Integer call() {
        return minimax(board, depth, player, alpha, beta);
    }

    private int minimax (int[] board, int depth, int player, int alpha, int beta) {
        int gameOverValue = getGameOverValue(depth);
        if (gameOverValue != -1) return gameOverValue;

        // TODO: Modify as command line args
        // if (depth == 100) return 0;

        return findBestVal(board, depth, player, alpha, beta);
    }

    private int getGameOverValue (int depth) {
        GameState state = ticTacToeBoard.isGameOver();
        switch (state) {
            case LOSE: // Player loses = Computer wins
                return 100 - depth;
            case WIN: // Player wins = Computer loses
                return -100 + depth;
            case TIE:
                return 0;
        }
        return -1;
    }

    private int findBestVal(int[] board, int depth, int player, int alpha, int beta) {
        int[] initialBoard = board.clone(); // Must clone here as will be modified when calling playAt(move, player)
        List<String> availMoves = getAvailMoves(board);
        List<Future<Integer>> results = new ArrayList<>();

        int bestVal;
        int otherPlayer;
        if (player == Game.O_PLAYER) { // Maximize
            bestVal = Integer.MIN_VALUE;
            otherPlayer = Game.X_PLAYER;
        } else {  // Minimize
            bestVal = Integer.MAX_VALUE;
            otherPlayer = Game.O_PLAYER;
        }
        for (int i = 0; i < availMoves.size(); i++) {
            String move = availMoves.get(i);
            ticTacToeBoard.playAt(move, player);
            Future<Integer> result = TicTacToe.executor.submit(new MinimaxTask(ticTacToeBoard.getBoard(), depth + 1, otherPlayer, alpha, beta));
            if (i == 0) {
                Bounds bounds = determineBounds(result, player, bestVal, alpha, beta);
                alpha = bounds.getAlpha();
                beta = bounds.getBeta();
                bestVal = bounds.getValue();
            } else results.add(result);

            ticTacToeBoard.setBoard(initialBoard.clone());
            if (beta <= alpha) break;
        }

        return getBestVal(results, bestVal);
    }

    private Bounds determineBounds (Future<Integer> result, int player, int bestVal, int alpha, int beta) {
        try {
            int val = result.get();
            bestVal = determineBestVal(player, bestVal, val);
            if (player == Game.O_PLAYER) alpha = Integer.max(alpha, bestVal);
            else beta = Integer.min(beta, bestVal);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return new Bounds(alpha, beta, bestVal);
    }

    private int getBestVal(List<Future<Integer>> results, int bestVal) {
        for (int i = 0; i < results.size(); i++) {
            try {
                Future<Integer> result = results.get(i);
                int val = result.get();
                bestVal = determineBestVal(player, bestVal, val);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        return bestVal;
    }

    private int determineBestVal (int player, int bestVal, int val) {
        if (player == Game.O_PLAYER) return Integer.max(bestVal, val);
        else return Integer.min(bestVal, val);
    }

    private List<String> getAvailMoves(int[] board) {
        List<String> availMoves = new ArrayList<>();
        for (int i = 0; i < board.length; i++) if (board[i] == ticTacToeBoard.FREE) availMoves.add(ticTacToeBoard.mapIndexToSquare(i));
        return availMoves;
    }
}
